#!/bin/bash

cd $HOME && \
mkdir .ssh && \
touch .ssh/authorized_keys && \
chmod 700 .ssh && \
chmod 640 .ssh/authorized_keys

