#!/bin/bash

EXPECTED_ARGS=2
if [ $# -ne $EXPECTED_ARGS ]; then
  echo "<<ERROR>> Usage: `basename $0` git_repo_name"
  exit $E_BADARGS
fi
USERNAME=$1
PASSWORD=$2

adduser $1
echo $1:$2 | chpasswd

