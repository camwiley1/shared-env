#!/bin/bash

# settings
CURL_ALIASES_URL='https://www.dropbox.com/s/eip0jatmgdwv24i/bash_aliases?dl=0'
SHARED_PATH='/home/shared'
DEVUSER='cameron'
DEVGROUP='developers'

printf "hi $USER. let's start building your server environment...\n"
printf "\n"

CUSTOM_PS1_TEST=`grep "CUSTOM_PS1" "$HOME/.bashrc"`

if [ $? -ne 0 ]; then
printf "\n"
printf "making your terminal font pretty..."
printf "\n"

cat << EOF | sudo tee -a "$HOME/.bashrc"

# Setting a nice custom prompt
CUSTOM_PS1="\[\e]0;\u@\h:\w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\n\$ "
export PS1=\$CUSTOM_PS1

# Setting vim as default editor 
export EDITOR=vim
EOF
    printf "[+] custom prompt added."
    printf "\n"
else
    printf "[-] custom prompt already exists. skipping..."
    printf "\n"
fi    

if [ ! -d "$SHARED_PATH" ]; then 
    mkdir $SHARED_PATH;
    mkdir $SHARED_PATH/config
    mkdir $SHARED_PATH/sites
    groupadd $DEVGROUP
    usermod -G $DEVGROUP $DEVUSER
    chgrp -R $DEVGROUP $SHARED_PATH 
    printf "[+] shared path added."
    printf "\n"
else
    printf "[-] shared path exists. skipping..."
    printf "\n"
fi    

if [ ! -f "$HOME/.bash_aliases" ]; then
    printf "\n"
    printf "fetching your aliases..."

    curl -L -o $HOME/bash_aliases_shared $CURL_ALIASES_URL
    cp bash_aliases_shared $HOME/.bash_aliases

    printf "\n"
    printf "[+] bash_aliases added."
else 
    printf "\n"
    printf "[-] bash_aliases already exists. skipping..."
    printf "\n"
fi

if [ ! -d "$HOME/.ssh" ]; then
    mkdir $HOME/.ssh
    chmod 700 $HOME/.ssh
    touch $HOME/.ssh/authorized_keys
    chmod 640 $HOME/.ssh/authorized_keys

    printf "\n"
    printf "[+] .ssh directory setup"
    printf "\n"
fi    

source $HOME/.bashrc

printf "\n"
printf "\n"
printf "ok. we're done here. now get to work slacker."
printf "\n"

