#!/bin/bash


EXPECTED_ARGS=1
if [ $# -ne $EXPECTED_ARGS ]; then
  echo "<<ERROR>> Usage: `basename $0` document_root"
  exit $E_BADARGS
fi


DOC_ROOT=$1
SCRIPT_PATH="/home/shared/environment/scripts/bash/"
WEB_PERM_FILE='web-permission-reset.sh'
SETTINGS_PATH="/sites/default/settings.php"

FULL_SETTINGS_PATH=${DOC_ROOT}${SETTINGS_PATH}
FULL_PERMISSION_PATH=${SCRIPT_PATH}${WEB_PERM_FILE}

sudo $FULL_PERMISSION_PATH

if [ -f "$FULL_SETTINGS_PATH" ]; then

  cd $DOC_ROOT && drush updatedb -y

fi

