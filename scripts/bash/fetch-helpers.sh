#!/usr/bin/env bash

printf "getting bash environment script...\n"
curl -L -o bash_setup.sh https://www.dropbox.com/s/y1viupn1eui9xs3/buildEnv.sh?dl=0
printf "\n"

printf "getting web environment script...\n"
curl -L -o install.sh https://www.dropbox.com/s/8e5yryh0erutok6/install.sh?dl=0
printf "\n"

printf "getting python make site script...\n"
curl -L -o makesite.py https://www.dropbox.com/s/z410cypdy7ei07x/makesite.py?dl=0
printf "\n"

