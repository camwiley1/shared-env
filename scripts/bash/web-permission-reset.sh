#!/bin/bash

SITEROOT='/home/shared/sites'
OWNER='www-data'
GROUP='web'

# update all site folder owner, group, and privileges for web
cd $SITEROOT && sudo chown -R $OWNER:$GROUP * && sudo chmod -R 775 *
