#!/usr/bin/env bash

GIT_USER='git'
GIT_PATH='/var/git'
GIT_REPO=$1
EXPECTED_ARGS=1
GIT_FULL_PATH="${GIT_PATH}/${GIT_REPO}"
GIT_REMOTE="ssh://git@${HOSTNAME}:390${GIT_PATH}/${GIT_REPO}"
SUCCESS_MESSAGE="<<new remote>> ${GIT_REMOTE}"

if [ $# -ne $EXPECTED_ARGS ]; then
  echo "<<ERROR>> Usage: `basename $0` git_repo_name"
  exit $E_BADARGS
fi

echo "creating new repo @ $GIT_FULL_PATH"

mkdir $GIT_FULL_PATH
cd $GIT_FULL_PATH
git --bare init

echo $SUCCESS_MESSAGE
echo 'done.'