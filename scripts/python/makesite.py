#!/usr/bin/env python
import os

# include php-info.php file for testing purposes
includePhpInfo = False

def ensureDirectory(directory):
    if not os.path.exists(directory):
        print("Directory not found, creating:")
        print(directory)
        os.makedirs(directory)
        print('.')
    else:
        print("Directory exists: "+directory)

def addPhpInfoFile(path):
    phpFile = os.path.join(path, 'php-info.php')
    if createNewFile(htmlFile, '<?php phpinfo(); ?>'):
        print("ALERT: php-info.php created for testing purposes. Please remove it once you confirm the site is working.")

def addIndexFile(path):
    htmlFile = os.path.join(path, 'index.html')
    if createNewFile(htmlFile, '<html><h2>'+siteName+'</h2><p>Awesome, html works for this site.</p></html>'):
        print("index.html generated in docment root.")

def createNewFile(filePath, content):
    if not os.path.isfile(filePath):
        f = open(filePath, 'w')
        f.write(content)
        f.close()
        return True
    return False

def addVhostInfo(vhostDir):

    vhostString = """
    <VirtualHost *:80>
        ServerAdmin %(admin)s
        ServerName %(site)s
        ServerAlias www.%(site)s
        DocumentRoot %(docRoot)s
        ErrorLog %(logPath)s/error.log
        CustomLog %(logPath)s/access.log combined
    </VirtualHost>
    """ % vhostData


    print('VirturalHost Text:')
    print('------------------------------')
    print(vhostString)
    print('------------------------------')

    vhostFile = os.path.join(vhostDir, siteName + '.conf')

    if createNewFile(vhostFile, vhostString):
        print("VirtualHost Written to "+vhostFile)
    else:
        print("VirtualHost already exists. NOT Writing vHost...")

baseDirectory = '/home/shared/sites/'
vhostDir = '/etc/apache2/sites-available/'
subdirs = ['httpdocs', 'httpsdocs', 'logs', 'data']

print("We will create a site at "+baseDirectory+"<site-name>")
print("Example Input: example.com -OR- sub.example.com")
print("Enter site-name:")

siteName = raw_input()
fullBaseDirectory = os.path.join(baseDirectory, siteName)

ensureDirectory(fullBaseDirectory)

for d in subdirs:
    newDir = os.path.join(fullBaseDirectory, d)
    ensureDirectory(newDir)

vhostData = {}
vhostData['docRoot'] = os.path.join(fullBaseDirectory, subdirs.pop(0))
vhostData['logPath'] = os.path.join(fullBaseDirectory, subdirs.pop(1))
vhostData['admin'] = 'admin@rescuescg.com'
vhostData['site'] = siteName

addVhostInfo(vhostDir)

addIndexFile(vhostData['docRoot'])

if includePhpInfo:
  addPhpInfoFile(vhostData['docRoot'])

os.system('a2ensite ' + siteName + '.conf')
print("Site enabled in apache.")

os.system('/etc/init.d/apache2 restart')

# Deprecated for web-permission-reset.sh command
#os.system('chown -R www-data '+fullBaseDirectory)

# Set correct web permissions for web directories
os.system('web-permission-reset.sh')

print("DONE!!!")
print("If the DNS is properly set, your site should be reachable at http://"+siteName)
