#!/usr/bin/env python
import os, subprocess

configName = '.bashrc'

if not os.path.isfile(configName):
    print ('error: ' + configName + ' not found. exit.');
    raise SystemExit

#exists = subprocess.check_output(["grep", 'tag:shared_env', configName])

#if exists:
#    print('script already run. exiting.')
#    raise SystemExit

sharedEnvStr = """
# shared environment info tag:shared_env
export SHARED_ENV=/home/shared/environment
export SHARED_SCRIPTS=$SHARED_ENV/scripts
export SHARED_CONFIG=$SHARED_ENV/config

if [ -f "$SHARED_CONFIG/.bash_profile" ]; then
    echo "#### SETTING SHARED ENVIRONMENT ####"
    source $SHARED_CONFIG/.bash_profile
fi

"""

print('appending shared environment info to .bash_profile')
print('****************************')

# python 2
fw = open(configName, 'a')
fw.write(sharedEnvStr)
fw.close()

print(sharedEnvStr)
print('****************************')
print('done.')