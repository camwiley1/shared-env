###############################################
#           Navigation & Meta                  #
###############################################
alias ll='ls -la'
alias l.='ls -d .*'
alias ..='cd ..'
alias ...='cd ../../'
alias gr_a='a | grep'
alias sa='source ~/.bash_aliases;source $SHARED_CONFIG/.bash_aliases;echo "Bash aliases sourced."'
alias a='echo "------------Your aliases------------";alias'
alias btail='tail -n 50'
alias h='history | tail -50'
alias ownWeb='sudo chown -R www-data'
alias chGrpDev='sudo chgrp -R developers .; chmod -R  g+rw .'
alias chGrpGit='sudo chgrp -R git .'
alias modWeb='sudo chmod -R 775'

##################################
# tar and gzip a directory
##################################
tarMake(){
tarMakeCommand="tar -czvf $1$(date +_%Y_%m_%d).tar.gz $1"
echo "Running: $tarMakeCommand"
tar -czvf $1$(date +_%Y_%m_%d).tar.gz $1
echo "Done..."
}

alias tarDir='tarMake'
alias tarExtract='tar -xzvf'

###############################################
#           Git Helpers                        #
###############################################
makeRepo() {
    cd $GIT_REPO && \
    mkdir /var/git/"$1" && \
    cd /var/git/"$1" && \
    git --bare init && \
    chGrpGit
    cd $GIT_REPO && modWeb $1
}

###############################################
#           Edit Files                        #
###############################################
alias changeEditor='sudo update-alternatives --config editor'
alias viashr='editor $SHARED_CONFIG/.bash_aliases'
alias viash='editor $SHARED_CONFIG/.bash_aliases'
alias viav='editor $SHARED_CONFIG/.vimrc'
alias viphp='editor /etc/php5/apache2/php.ini'
alias viapache='editor /etc/apache2/apache2.conf'

###############################################
#           Restart Services                  #
###############################################
alias sshrestart='sudo /etc/init.d/ssh restart'
alias apacherestart='sudo /etc/init.d/apache2 restart'
alias mysqlrestart='sudo /etc/init.d/mysql restart'
alias svnrestart='svnserve -d -r /home/shared/svn'

alias psUptime="ps -eo pid,args | grep [u]ptimeMonitor.py | awk '{print $1}'"
alias pscust="ps -eo pid,args | grep $1"
alias psSoffice="pscust [s]office"
alias uptimeMonitorKill="kill psUptime"

###############################################
#           Location
###############################################
alias goapache='cd /etc/apache2'
alias goshared='cd $SHARED_ENV'
alias goscripts='cd $SHARED_SCRIPTS'
alias goconfig='cd $SHARED_CONFIG'
alias gosites='cd $SHARED_SITES'

