echo "reading shared .bashrc"

# START MEMCACHED
#echo "starting memcached..."
#memcached&

#HISTORY SETTINGS
export HISTSIZE=500
export HISTFILESIZE=1000

# SET PROMPT
export PS1="\[\e]0;\u@\h:\w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\n\$ "

# SET ALIASES
if [ -f $SHARED_CONFIG/.bash_aliases ]; then
    source $SHARED_CONFIG/.bash_aliases
fi

