echo "reading shared .bash_profile"

# set in /home/<user>/.bash_profile
export SHARED=/home/shared
export SHARED_SITES=$SHARED/sites
export SHARED_ENV=/home/shared/environment
export SHARED_CONFIG=$SHARED_ENV/config
export SHARED_SCRIPTS=$SHARED_ENV/scripts

# PATH
PATH_MATCH=$(echo $PATH | grep $SHARED_SCRIPTS)
if [ "$PATH_MATCH" == "" ]; then
    export PATH=$SHARED_SCRIPTS/bash:$PATH
    export PATH=$SHARED_SCRIPTS/python:$PATH
    export PATH=$SHARED_SCRIPTS/php:$PATH
fi

if [ -f $SHARED_CONFIG/.bashrc ]; then
   source $SHARED_CONFIG/.bashrc
fi


