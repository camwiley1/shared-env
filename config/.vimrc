" I like 4 spaces for indenting
set shiftwidth=4

" I like 4 stops
set tabstop=4
"
" " Spaces instead of tabs
set expandtab

" Always  set auto indenting on
set autoindent